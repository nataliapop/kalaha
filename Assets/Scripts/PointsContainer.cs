﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
//Project created using Unity 2018.03.11f1

public enum boardObjectType { BaseDown, BaseUp, HoleDown, HoleUp }
public class PointsContainer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { }

    public boardObjectType BoardObjectType;
    public GameObject StonePrefab;
    public int Points
    {
        get
        {
            if (gameObject.transform.childCount == 0)
            { return 0; }
            if (gameObject.transform.GetChild(gameObject.transform.childCount - 1)
                   .GetComponentInChildren<Stone>().IsMovingAnywhere)
            { return gameObject.transform.childCount - 1; }
            return gameObject.transform.childCount;
        }
    }

    public void AddOneStoneThroughTempCont(Transform newStone)
    {
        AddOneStoneDependingOnBoardObjectType(newStone, true);
    }

    public void AddOneStoneDirectly(Transform newStone)
    {
        AddOneStoneDependingOnBoardObjectType(newStone, false);
    }

    private void AddOneStoneDependingOnBoardObjectType(Transform newStone, bool moveToTempCont)
    {
        if (BoardObjectType == boardObjectType.BaseDown || BoardObjectType == boardObjectType.BaseUp)
        {
            newStone.GetComponent<Stone>().SetNewTargetLocalPositionAndNewParentTransform
                (new Vector3(Random.Range(-0.2f, 0.2f), Random.Range(-1.0f, 1.0f)),
                gameObject.transform, moveToTempCont);
            return;
        }
        else
        {//params for Holes and TempCont
            newStone.GetComponent<Stone>().SetNewTargetLocalPositionAndNewParentTransform
                (new Vector3(Random.Range(-0.4f, 0.4f), Random.Range(-0.3f, 0.3f)),
                gameObject.transform, moveToTempCont);
            return;
        }
    }

    public List<Transform> ListOfAllMyStones()
    {
        var ListOfStones = new List<Transform>();
        for (int i = 0; i < transform.childCount; i++)
        { ListOfStones.Add(transform.GetChild(i)); }
        return ListOfStones;
    }

    // Update is called once per frame
    void Update()
    { }
}
