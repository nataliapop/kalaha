﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Project created using Unity 2018.03.11f1

public class TemporaryContainer : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { }

    public Stone currentlyMovingStone = null;
    public bool specialMove = false;
    public int lastFieldIndex = 0;

    // Update is called once per frame
    void Update()
    {
        if (currentlyMovingStone != null
            && currentlyMovingStone.GetComponent<Stone>().IsMovingAnywhere)
        { return; }
        currentlyMovingStone = null;

        if (gameObject.transform.childCount == 0)
        { return; }

        foreach (Transform stone in gameObject.transform)
        {
            if (stone.gameObject.GetComponent<Stone>().IsMovingAnywhere)
            {
                currentlyMovingStone = stone.GetComponent<Stone>();
                return;
            }
            currentlyMovingStone = null;
        }

        if (currentlyMovingStone == null)
        {
            currentlyMovingStone = gameObject.transform.GetChild(0).GetComponent<Stone>();
            if (gameObject.transform.childCount == 1 && specialMove)
            {//this stone right now is not our child anymore
                currentlyMovingStone.StartMovingToTargetPosition(true, lastFieldIndex);
                //Debug.Log("Temporarycontainer lastFieldIndex: " + lastFieldIndex);
                specialMove = false;
            }
            else
            { currentlyMovingStone.StartMovingToTargetPosition(false, lastFieldIndex); }
        }
    }
}
