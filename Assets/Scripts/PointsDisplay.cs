﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsDisplay : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public PointsContainer pointsContainer;

    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = pointsContainer.Points.ToString();
    }
}
