﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
//Project created using Unity 2018.03.11f1

public class TurnDisplay : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
    public GameObject WinLosePrefab;
    private PlayerConnectionObject myConnectionObject;
    // Update is called once per frame
    void Update()
    {
        if (myConnectionObject == null)
        {
            myConnectionObject = GameObject.FindObjectsOfType<PlayerConnectionObject>()
                  .Where(x => x.isLocalPlayer).FirstOrDefault();
            return;
        }

        //if (GameObject.FindGameObjectWithTag("TempCont")
        //       .GetComponent<TemporaryContainer>().currentlyMovingStone != null)
        if (GameObject.FindGameObjectWithTag("TempCont").transform.childCount > 0)
        { return; }
        if (GameObject.FindObjectsOfType<Stone>().Where(x => x.IsMovingAnywhere).Count() > 0)
        { return; }

        GetComponent<Text>().text = myConnectionObject.MyTurn.ToString();
        EndGameBecauseOfWin();
        EndGameBecauseOfNoStones();
    }

    private void EndGameBecauseOfWin()
    {
        if (GameObject.FindObjectsOfType<WinLose>().ToList().Count > 0)
        { return; }

        var baseUpPoints = GameObject.FindObjectsOfType<PointsContainer>()
              .Where(x => x.BoardObjectType == boardObjectType.BaseUp).First().Points;
        var baseDownPoints = GameObject.FindObjectsOfType<PointsContainer>()
              .Where(x => x.BoardObjectType == boardObjectType.BaseDown).First().Points;
        if (baseUpPoints > 24 || baseDownPoints > 24
            || (baseUpPoints == 24 && baseDownPoints == 24))
        {
            var winLose = Instantiate(WinLosePrefab);
        }
    }

    private void EndGameBecauseOfNoStones()
    {
        //we must check if any player has no stones. If so, the game ends and all resting stones goes to the nearest base.
        var fieldsDown = GameObject.FindObjectsOfType<PointsDistributor>()
            .Where(x => x.BoardObjectType == boardObjectType.HoleDown);
        var emptyFieldsDown = fieldsDown.Where(x => x.Points == 0);
        //var emptyFieldsDown = fieldsDown.Where(x => x.transform.childCount == 0);

        var fieldsUp = GameObject.FindObjectsOfType<PointsDistributor>()
            .Where(x => x.BoardObjectType == boardObjectType.HoleUp);
        var emptyFieldsUp = fieldsUp.Where(x => x.Points == 0);
        //var emptyFieldsUp = fieldsUp.Where(x => x.transform.childCount == 0);

        var fieldsManager = GameObject.FindObjectOfType<FieldsManager>();
        //-------------//
        var fields = fieldsUp;
        var searchedBase = boardObjectType.BaseUp;
        var nearestBase = fieldsManager.FindBoardObject(searchedBase);
        if (emptyFieldsDown.Count() == 6)
        {
        }
        else if (emptyFieldsUp.Count() == 6)
        {
            fields = fieldsDown;
            searchedBase = boardObjectType.BaseDown;
            nearestBase = fieldsManager.FindBoardObject(searchedBase);
        }
        else
        { return; }

        foreach (var field in fields)
        {
            var listOfStones = field.ListOfAllMyStones();
            while (listOfStones.Count > 0)
            {
                var lastStoneIndex = listOfStones.Count - 1;
                nearestBase.AddOneStoneDirectly(listOfStones[lastStoneIndex]);
                listOfStones.RemoveAt(lastStoneIndex);
            }
        }
    }
}
