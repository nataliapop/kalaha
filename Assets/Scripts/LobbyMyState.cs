﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
//Project created using Unity 2018.03.11f1

public class LobbyMyState : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { }

    private PlayerConnectionObject myObj;
    // Update is called once per frame
    void Update()
    { }

    public void ButtonReadyStaneChange()
    {
        print("Klik!");
        if (myObj == null)
        {
            myObj = GameObject.FindObjectsOfType<PlayerConnectionObject>()
                .Where(x => x.isLocalPlayer == true).First();
        }
        if (myObj == null)
        { return; }
        print("Teraz jest " + myObj.IsReady);
        print("I chce zmienic na " + !myObj.IsReady);
        myObj.CmdChangeReadyStane(!myObj.IsReady);
    }

    public void UpdateText(bool readyState)
    {
        print("MyStateText == " + gameObject.GetComponent<Text>().text);
        print("Zmieniam MyStateText na " + readyState);
        if (readyState)
        {
            gameObject.GetComponent<Text>().text = "Ja: gotowy.";
        }
        else
        {
            gameObject.GetComponent<Text>().text = "Ja: niegotowy.";
        }
    }
}
