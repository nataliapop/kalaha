﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using UnityEngine.UI;

public class PointsDistributor : PointsContainer
{
    // Start is called before the first frame update
    void Start()
    {
        spawnInitialStones();
        //fieldsManager = GameObject.FindObjectOfType<FieldsManager>();
        //turnDisplay = GameObject.FindObjectOfType<TurnDisplay>().transform;
    }
    
    public void DistributeStones()
    {
        //Debug.Log("Client: Searching for myConnectionObject.");
        PlayerConnectionObject myConnectionObject =
            GameObject.FindObjectsOfType<PlayerConnectionObject>()
                .Where(x => x.isLocalPlayer).FirstOrDefault();

        //ask for move
        FieldsManager fieldsManager = GameObject.FindObjectOfType<FieldsManager>();
        var myIndex = System.Array.IndexOf(fieldsManager.arrayOfFields, this);
        if (myConnectionObject == null)
        {
            //Debug.Log("Client: myConnectionObject is still null");
            return;
        }
        if(GameObject.FindObjectOfType<TurnDisplay>().transform.GetComponent<Text>().text == "False")
        //if (myConnectionObject.myTurn == false)
        {
            //Debug.Log("Client: myConnectionObject.myTurn = " + myConnectionObject.myTurn);
            return;
        }

        if (GameObject.FindGameObjectWithTag("TempCont")
               .GetComponent<TemporaryContainer>().currentlyMovingStone != null)
        {
            //Debug.Log("Client: Previous move is still running");
            return;
        }

        if (fieldsManager.arrayOfFields[myIndex].Points == 0)
        {
            //Debug.Log("Client: Field points == 0");
            return;
        }

        //Debug.Log("Client: I am asking for move");
        myConnectionObject.CmdMakePlayerMove(myIndex);
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void spawnInitialStones()
    {
        for (int i = 0; i < 4; i++)
        {
            var newStone = Instantiate(StonePrefab, gameObject.transform);
            newStone.transform.SetParent(gameObject.transform);
            float diff = 0.2f;
            float xOffset = 0.0f;
            float yOffset = 0.0f;
            switch (i)
            {
                case 0:
                    {
                        xOffset = -diff;
                        yOffset = -diff;
                        break;
                    }
                case 1:
                    {
                        xOffset = -diff;
                        yOffset = diff;
                        break;
                    }
                case 2:
                    {
                        xOffset = diff;
                        yOffset = diff;
                        break;
                    }
                case 3:
                    {
                        xOffset = diff;
                        yOffset = -diff;
                        break;
                    }
                default: { break; }
            }
            newStone.transform.localPosition =
                new Vector3(newStone.transform.localPosition[0] + xOffset,
                newStone.transform.localPosition[1] + yOffset,
                 newStone.transform.localPosition[2]);
        }
    }
}
