﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using System.Linq;
using UnityEngine.UI;
//Project created using Unity 2018.03.11f1

public class PlayerConnectionObject : NetworkBehaviour
{
    // Start is called before the first frame update
    void Start()
    {/*
        if (isServer)
        {
            var PCOs = GameObject.FindObjectsOfType<PlayerConnectionObject>().ToList();
            foreach (var obj in PCOs)
            { obj.RpcVerifyNumberOfPlayersAndScene(PCOs.Count()); }
        }*/
        if (isLocalPlayer)
        { CmdChangeReadyStane(isReady); print("Tworze nowy obiekt"); }
    }

    private bool isSceneLoading = false;
    private AsyncOperation sceneAsync = null;
    private FieldsManager fieldsManager;

    [SyncVar]
    public string PlayerName;

    [SyncVar]
    private bool myTurn = false;
    public bool MyTurn { get => myTurn; }

    [SyncVar(hook = "isReadyHook")]
    public bool isReady = false;
    public bool IsReady { get => isReady; }

    void Update()// Update is called once per frame
    { }
    /*
    void OnDestroy()
    {
        //ValidateDisconnecting();
        print("niszcze");
        var restOfPCOs = GameObject.FindObjectsOfType<PlayerConnectionObject>();
        if (restOfPCOs.Count() > 0)
        {
            restOfPCOs.Where(x => x.isLocalPlayer == true).First()
              .VerifyNumberOfPlayersAndScene();
        }
    }*/
    public void isReadyHook(bool value)
    {
        //print("TU");
        isReady = value;
    }

    [Command]
    public void CmdChangeReadyStane(bool value)
    {
        //print("Serwer: mam zmienic stan isLocal " + isLocalPlayer + " z " + isReady + " na " + value);
        isReady = value;
        //print("Serwer: teraz " + isReady + " == " + value);
        RpcReadyStateChanged(value);
    }

    [ClientRpc]
    public void RpcReadyStateChanged(bool value)
    {
        isReady = value;
        //print("RpcReadyStateChanged isReady == " + isReady);
        //print("isLocalPlayer " + isLocalPlayer);
        if (SceneManager.GetActiveScene().name == "LobbyScene")
        {
            var myStateTextObj = GameObject.FindGameObjectWithTag("MyStateTag")
                .GetComponent<LobbyMyState>();
            var myPCO = GameObject.FindObjectsOfType<PlayerConnectionObject>()
                .Where(x => x.isLocalPlayer == true).FirstOrDefault();
            //print("myPCO " + myPCO.IsReady);

            var friendStateTextObj = GameObject.FindGameObjectWithTag("FriendStateTag")
                .GetComponent<LobbyFriendState>();
            var FriendPCO = GameObject.FindObjectsOfType<PlayerConnectionObject>()
                .Where(x => x.isLocalPlayer == false).FirstOrDefault();
            //if (FriendPCO != null)
            //{ print("FriendPCO " + FriendPCO.IsReady); }
            //else
            //{ print("Nie mam Frienda"); }

            //print("przed " + myStateTextObj.GetComponent<Text>().text);
            myStateTextObj.UpdateText(myPCO.IsReady);
            //print("po " + myStateTextObj.GetComponent<Text>().text);
            if (FriendPCO == null)
            { return; }
            friendStateTextObj.UpdateText(FriendPCO.IsReady);
        }

        //Do the things you want to do in the Hook, like call another function or something
        var readyPCOs = GameObject.FindObjectsOfType<PlayerConnectionObject>()
        .Where(x => x.IsReady == true);
        if (readyPCOs.Count() == 2)
        {
            print("Czy mozemy juz grac???????????????????????????????????????");
            readyPCOs.Where(x => x.isLocalPlayer == true).First()
                .VerifyNumberOfPlayersAndScene();
            /*
        foreach (var obj in PCOs)
        { obj.RpcVerifyNumberOfPlayersAndScene(); }*/
        }
    }

    //[ClientRpc]
    public void VerifyNumberOfPlayersAndScene()
    {
        print("RpcVerifyNumberOfPlayersAndScene");
        var PCOs = GameObject.FindObjectsOfType<PlayerConnectionObject>().ToList();
        //var ReadyPCOs = PCOs.Where(x => x.IsReady == true);
        if (isLocalPlayer == false)
        { return; }
        var sceneName = string.Empty;
        if (SceneManager.GetActiveScene().name == "LobbyScene")// && ReadyPCOs.Count() == 2)
        {
            sceneName = "MainScene";
            CmdEstablishTurn();
        }
        else if (SceneManager.GetActiveScene().name == "MainScene" && PCOs.Count() == 1)
        {
            sceneName = "LobbyScene";
            print("Zaladuje lobby scene");
        }

        if (sceneName == string.Empty || isSceneLoading == true)
        { return; }

        isSceneLoading = true;
        CmdChangeReadyStane(false);
        StartCoroutine(ManageScenes(sceneName));
    }

    //////////Commands////////////////////
    // Commands are special functions that ONLY get executed on the server.

    [Command]
    private void CmdEstablishTurn()
    {
        var PCOs = GameObject.FindObjectsOfType<PlayerConnectionObject>();
        if (PCOs[0] == this)
        {
            PlayerName += "1";
            myTurn = true;
        }
        else if (PCOs[1] == this)
        {
            PlayerName += "2";
            myTurn = false;
        }
        else
        {
            //Debug.Log("Server: ID " + NetworkServer.connections.Count + " player has been removed.");
            NetworkServer.connections[NetworkServer.connections.Count - 1].Disconnect();
        }
    }

    [Command]
    public void CmdGoToLobby()
    {
        RpcGoToLobby();
    }

    [ClientRpc]
    public void RpcGoToLobby()
    {
        if (!(SceneManager.GetActiveScene().name == "MainScene"))
        { return; }

        print("RpcGoToLobby");
        isSceneLoading = true;
        CmdChangeReadyStane(false);
        StartCoroutine(ManageScenes("LobbyScene"));
        /*
        print("rozlaczanie");
        if (isLocalPlayer)
        { NetworkManager.singleton.client.Disconnect(); }*/
    }

    private IEnumerator ManageScenes(string sceneName)
    {
        //print("Loading started for " + sceneName);
        AsyncOperation scene = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        scene.allowSceneActivation = false;
        sceneAsync = scene;

        //Wait until we are done loading the scene
        while (scene.progress < 0.9f)
        {
            //print("Loading scene " + " [][] Progress: " + scene.progress);
            yield return null;
        }
        OnFinishedLoadingAllScene(sceneName);
    }

    private void OnFinishedLoadingAllScene(string sceneName)
    {
        //Debug.Log("Done Loading Scene");
        StartCoroutine(enableScene(sceneName));
        //Debug.Log("Scene Activated!");
    }

    private IEnumerator enableScene(string sceneName)
    {
        //Activate the Scene
        sceneAsync.allowSceneActivation = true;

        Scene sceneToLoad = SceneManager.GetSceneByName(sceneName);
        if (sceneToLoad.IsValid())
        {
            //Debug.Log("Scene is Valid");
            while (sceneToLoad.isLoaded == false)
            { yield return null; }
            var PCO = GameObject.FindObjectsOfType<PlayerConnectionObject>().ToList();
            foreach (var obj in PCO)
            { SceneManager.MoveGameObjectToScene(obj.gameObject, sceneToLoad); }
            var currentSceneName = SceneManager.GetActiveScene().name;
            //print("currentSceneName: " + currentSceneName);
            SceneManager.SetActiveScene(sceneToLoad);
            SceneManager.UnloadScene(currentSceneName);
            isSceneLoading = false;
            if (SceneManager.GetActiveScene().name == "MainScene")
            { CmdChangeReadyStane(true); }
            else
            { CmdChangeReadyStane(false); }
        }
    }

    [Command]
    public void CmdMakePlayerMove(int buttonIndex)
    {/*
        if (GameObject.FindObjectsOfType<PlayerConnectionObject>()
            .Where(x => x.isReady == true).Count() < 2)
        {
            //RpcDebugLogMessageToLocalPlayer("Server to: " + PlayerName + " - second player is not ready yet");
            return;
        }*/
        if (myTurn == false)
        {
            //Debug.Log("Server: " + PlayerName + " - it is not your turn");
            //RpcDebugLogMessageToLocalPlayer("Server to: " + PlayerName + " - it is not your turn");
            return;
        }
        var message = PlayerName + " is making a move";
        //Debug.Log("Server: " + message);
        //RpcDebugLogMessage("Client: " + message);
        RpcMakePlayerMove(buttonIndex);
    }

    [Command]
    public void CmdPlayerFinishedMove(bool isNextTurnMine)
    {
        if (isNextTurnMine)
        { return; }
        var PlayerConnectionObjectArray = GameObject.FindObjectsOfType<PlayerConnectionObject>();
        foreach (var obj in PlayerConnectionObjectArray)
        {
            obj.myTurn = !obj.myTurn;
        }
    }

    ////////////RPC
    // RPCs are special function that ONLY get executed on the clients.
    /*
    [ClientRpc]
    public void RpcDebugLogMessage(string message)
    { Debug.Log(message); }

    [ClientRpc]
    public void RpcDebugLogMessageToLocalPlayer(string message)
    { if (isLocalPlayer) { Debug.Log(message); } }
    */
    [ClientRpc]
    public void RpcMakePlayerMove(int buttonIndex)
    {
        int offset = 0;
        if (isLocalPlayer == false)
        {
            offset = 7;
        }
        if (fieldsManager == null)
        { fieldsManager = GameObject.FindObjectOfType<FieldsManager>(); }
        var numberOfFields = fieldsManager.arrayOfFields.Length;
        var listOfFreeStones =
            fieldsManager.arrayOfFields[(buttonIndex + offset) % numberOfFields].ListOfAllMyStones();
        //assigning free stones to new parents, exactly to TempCont
        int lastFieldIndex = 0;
        for (var fieldNumber = buttonIndex + offset + 1; listOfFreeStones.Count > 0; fieldNumber++)
        {
            var lastStoneIndex = listOfFreeStones.Count - 1;
            fieldsManager.arrayOfFields[fieldNumber % numberOfFields].
                AddOneStoneThroughTempCont(listOfFreeStones[lastStoneIndex]);

            listOfFreeStones.RemoveAt(lastStoneIndex);
            lastFieldIndex = fieldNumber;
        }

        if (isLocalPlayer
            && fieldsManager.arrayOfFields[lastFieldIndex % numberOfFields].Points == 0
            && fieldsManager.arrayOfFields[lastFieldIndex % numberOfFields]
            .BoardObjectType == boardObjectType.HoleDown)
        {
            specialMove(lastFieldIndex % numberOfFields);
            //Debug.Log("Client: my special move\nlastFieldIndex: " + lastFieldIndex);
        }

        if (isLocalPlayer == false
            && fieldsManager.arrayOfFields[lastFieldIndex % numberOfFields].Points == 0
            && fieldsManager.arrayOfFields[lastFieldIndex % numberOfFields]
            .BoardObjectType == boardObjectType.HoleUp)
        {
            specialMove(lastFieldIndex % numberOfFields);
            //Debug.Log("Client: Other player special move\nlastFieldIndex: " + lastFieldIndex);
        }

        if (isLocalPlayer)
        {
            if (fieldsManager.arrayOfFields[lastFieldIndex % numberOfFields]
            .BoardObjectType == boardObjectType.BaseDown)
            {
                CmdPlayerFinishedMove(myTurn);
                return;
            }
            CmdPlayerFinishedMove(!myTurn);
        }
    }

    private void specialMove(int lastFieldIndex)
    {
        var tempCont = GameObject.FindGameObjectWithTag("TempCont")
            .transform.GetComponent<TemporaryContainer>();
        tempCont.specialMove = true;
        tempCont.lastFieldIndex = lastFieldIndex;
        //Debug.Log("Special move has been executed");
    }
}
