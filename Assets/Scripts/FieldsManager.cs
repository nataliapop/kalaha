﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
//Project created using Unity 2018.03.11f1

public class FieldsManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    public PointsContainer[] arrayOfFields;

    public int FindNearestBaseFromIndex(int startSearchIndex)//does not check if actual field is a base. Starts searching from next one
    {
        int nearestBaseFieldIndex = startSearchIndex + 1;
        var numberOfFields = arrayOfFields.Length;
        //iteration is necessary becouse we search here for nearest base from specified field
        //rather than nearest from the beginning of collection
        for (; arrayOfFields[nearestBaseFieldIndex % numberOfFields]; nearestBaseFieldIndex++)
        {
            if (arrayOfFields[nearestBaseFieldIndex % numberOfFields].BoardObjectType == boardObjectType.BaseDown
                || arrayOfFields[nearestBaseFieldIndex % numberOfFields].BoardObjectType == boardObjectType.BaseUp)
            { break; }
        }
        return nearestBaseFieldIndex % numberOfFields;
    }

    public PointsContainer FindBoardObject(boardObjectType boardObjectType)
    { return GameObject.FindObjectsOfType<PointsContainer>()
            .Where(x => x.BoardObjectType == boardObjectType).First(); }

    // Update is called once per frame
    void Update()
    {

    }
}
