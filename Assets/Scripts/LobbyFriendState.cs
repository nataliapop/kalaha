﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
//Project created using Unity 2018.03.11f1

public class LobbyFriendState : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { }

    // Update is called once per frame
    void Update()
    { }

    public void UpdateText(bool readyState)
    {
        print("Zmieniam FriendStateText na " + readyState);
        if (readyState)
        {
            gameObject.GetComponent<Text>().text = "Drugi gracz: gotowy.";
        }
        else
        {
            gameObject.GetComponent<Text>().text = "Drugi gracz: niegotowy.";
        }
    }
}

