﻿using UnityEngine;

public class Stone : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var color = Random.ColorHSV();
        var r = Random.Range(0.2f, 1);
        var g = Random.Range(0.2f, 1);
        var b = Random.Range(0.2f, 1);
        GetComponent<SpriteRenderer>().color = new Color(r, g, b);
        targetLocalPosition = gameObject.transform.localPosition;
        tempContTransform = GameObject.FindGameObjectWithTag("TempCont").transform;
        //GetComponent<SpriteRenderer>().color = color;
        //Debug.Log("r: " + r + " g: " + g + " b: " + b);
    }

    private Transform tempContTransform;
    //private Vector3 tempContTransformPosition;

    private Vector3 targetLocalPosition;
    private Transform targetParentTransform;
    private Vector3 velocity;
    private float smoothTime = 0.2f;
    private bool specialMove;
    private int thisStoneFieldIndex;
    public bool IsStoneMovingToTempContainer { get; private set; } = false;
    public bool IsStoneMovingToTargetParent { get; private set; } = false;
    public bool IsMovingAnywhere
    {
        get { return IsStoneMovingToTempContainer || IsStoneMovingToTargetParent; }
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.localPosition.ToString() != targetLocalPosition.ToString())
        //if (gameObject.transform.localPosition != targetLocalPosition)
        {
            gameObject.transform.localPosition =
                Vector3.SmoothDamp(gameObject.transform.localPosition,
                targetLocalPosition, ref velocity, smoothTime);
        }
        else
        {
            IsStoneMovingToTargetParent = false;
            IsStoneMovingToTempContainer = false;
            if (specialMove == false)
            { return; }

            specialMove = false;
            //searching for field index where this last stone is
            //then searching for nearest base field

            var fieldsManager = GameObject.FindObjectOfType<FieldsManager>();
            var nearestBaseFieldIndex = fieldsManager.FindNearestBaseFromIndex(thisStoneFieldIndex);
            var numberOfFields = fieldsManager.arrayOfFields.Length;

            var distance = nearestBaseFieldIndex - thisStoneFieldIndex;
            var oppositeStonesFieldIndex = (nearestBaseFieldIndex + distance + numberOfFields) % numberOfFields;
            if (fieldsManager.arrayOfFields[oppositeStonesFieldIndex].Points == 0)
            { return; }//if there is no stone, special move does not execute

            //remove all stones z pola tego kamienia oraz z przeciwleglego
            {
                //we move this stone that owes this script to the base
                var listOfThisStone = fieldsManager.arrayOfFields[thisStoneFieldIndex].ListOfAllMyStones();
                //Debug.Log("Na polu " + thisStoneFieldIndex + " jest tyle kamieni: " + listOfThisStone.Count);
                fieldsManager.arrayOfFields[nearestBaseFieldIndex]
                    .AddOneStoneDirectly(listOfThisStone[0]);
                listOfThisStone.RemoveAt(0);
                //Debug.Log("Ostatni kamien z pola " + thisStoneFieldIndex + " zostal skierowany do bazy");
            }

            //we move all oposite stones to the base
            var listOfOppositeStones = fieldsManager.arrayOfFields[oppositeStonesFieldIndex]
                .ListOfAllMyStones();
            while (listOfOppositeStones.Count > 0)
            {
                var lastStoneIndex = listOfOppositeStones.Count - 1;
                fieldsManager.arrayOfFields[nearestBaseFieldIndex]
                    .AddOneStoneDirectly(listOfOppositeStones[lastStoneIndex]);
                listOfOppositeStones.RemoveAt(lastStoneIndex);
                //Debug.Log("Kamien " + lastStoneIndex + " z pola " + (oppositeStonesFieldIndex) + " zostal skierowany do bazy");
            }
        }
    }

    public void StartMovingToTargetPosition(bool isSpecialMove, int LastFieldIndex)
    {
        IsStoneMovingToTargetParent = true;
        IsStoneMovingToTempContainer = false;
        specialMove = isSpecialMove;
        thisStoneFieldIndex = LastFieldIndex;
        //Debug.Log("isSpecialMove: " + isSpecialMove + "\nLastFieldIndex: " + LastFieldIndex);
        gameObject.transform.SetParent(targetParentTransform);
    }

    public void SetNewTargetLocalPositionAndNewParentTransform
        (Vector3 newLocalPosition, Transform newParentTransform, bool moveToTempCont)
    {
        targetParentTransform = newParentTransform;
        targetLocalPosition = newLocalPosition;
        velocity = Vector3.zero;
        if (moveToTempCont)
        {
            IsStoneMovingToTempContainer = true;
            IsStoneMovingToTargetParent = false;
            gameObject.transform.SetParent(tempContTransform);
        }
        else
        {
            StartMovingToTargetPosition(false, thisStoneFieldIndex);
        }
    }
}
