﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
//Project created using Unity 2018.03.11f1
public class Exit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    public void ExitAction()
    {
        var myPCO = GameObject.FindObjectsOfType<PlayerConnectionObject>()
            .Where(x => x.isLocalPlayer).First();
        myPCO.CmdGoToLobby();
        //SceneManager.LoadScene("WelcomeScene");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
