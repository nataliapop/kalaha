﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public enum WinLoseTie { Win, Lose, Tie }
public class WinLose : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start");
        if (GameObject.FindObjectsOfType<PointsContainer>()
              .Where(x => x.BoardObjectType == boardObjectType.BaseDown).First().Points > 24)
        { setWinLose(WinLoseTie.Win); }
        else if (GameObject.FindObjectsOfType<PointsContainer>()
         .Where(x => x.BoardObjectType == boardObjectType.BaseUp).First().Points > 24)
        { setWinLose(WinLoseTie.Lose); }
        else
        { setWinLose(WinLoseTie.Tie); }
    }

    private void setWinLose(WinLoseTie result)
    {
        string Text = string.Empty;
        switch (result)
        {
            case WinLoseTie.Win:
                {
                    Text = "Wygrałeś!";
                    break;
                }
            case WinLoseTie.Lose:
                {
                    Text = "Przegrałeś!";
                    break;
                }
            case WinLoseTie.Tie:
                {
                    Text = "Remis!";
                    break;
                }
        }
        Debug.Log("setWinLose: " + Text);
        GetComponent<Text>().text = Text;
        Debug.Log("GetComponent<Text>().text = : " + GetComponent<Text>().text);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
